<?php
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END;

$c = $this->getId();
$a = $this->getAction()->getId();
//echo Yii::app()->user->id; 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/datepicker.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/matrix-style.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/matrix-media.css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.gritter.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-wysihtml5.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.tokenize.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.gritter.css" /> 
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.modal.css" /> 
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/message.css" />
        <style>
            .Zebra_DatePicker {
                z-index:99999999999;
            }
        </style>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/excanvas.min.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.min.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.ui.custom.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/wysihtml5-0.3.0.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.peity.min.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-wysihtml5.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.validate.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.wizard.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/matrix.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/matrix.wizard.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zebra_datepicker.src.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.chained.min.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.tokenize.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.printPage.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.modal.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tinymce/tinymce.min.js"></script> 
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.gritter.min.js"></script> 
    </head>

    <body>
        <!--Header-part-->
        <div id="header">
     
        </div>
        <!--close-Header-part--> 

        <!--top-Header-menu-->
        <div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav">
                <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Account</span><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo Yii::app()->createUrl('site/myprofile'); ?>"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/editprofile'); ?>"><i class="icon-edit"></i> Edit Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/login'); ?>"><i class="icon-key"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--close-top-Header-menu-->
        <!--start-top-serch-->

        <!--close-top-serch-->
        <!--sidebar-menu-->
        <div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
            <ul>
                <li <?php if ($this->id == "site" && $this->getAction()->id == "index"): ?> class="active" <?php endif; ?>><a href="<?php echo Yii::app()->createUrl('site/index'); ?>"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>


                <li <?php if ($this->id == "customer"): ?> class="active" <?php endif; ?>> <a href="<?php echo Yii::app()->createUrl('customer/admin'); ?>"><i class="icon icon-list"></i> <span>Customer</span></a> </li>
                <li <?php if ($this->id == "User"): ?> class="active" <?php endif; ?>> <a href="<?php echo Yii::app()->createUrl('User/admin'); ?>"><i class="icon icon-fullscreen"></i> <span>User</span></a> </li>
            </ul>
        </div>
        <!--sidebar-menu-->

        <!--main-container-part-->
        <div id="content">
            <!--breadcrumbs-->
            <div id="content-header">
                <div id="breadcrumb"> <a href="<?php echo Yii::app()->createUrl('site/index'); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
            </div>
            <!--End-breadcrumbs-->
            <div style="padding:10px">
                <?php echo $content; ?>
            </div>

        </div>

        <!--end-main-container-part-->

        <!--Footer-part-->

        <div class="row-fluid">
            <div id="footer" class="span12"> TES PHP</div>
        </div>

        <!--end-Footer-part-->
        
        <script>
            window.notif = new Audio('<?php echo Yii::app()->baseUrl; ?>/notif.mp3');
            
            
            function updateNotifications() {
                var controller = '<?php echo $c; ?>';
                var action = '<?php echo $a; ?>';
                var message_icon = '<?php echo Yii::app()->baseUrl; ?>/images/envelope.png';
            }

            setInterval(updateNotifications, 2000);

            $(document).ready(function () {
                
            });
        </script>
    </body>
</html>
