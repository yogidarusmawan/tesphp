<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property integer $id_c
 * @property integer $id_s
 * @property integer $id_u
 * @property string $nama
 * @property string $kota
 * @property string $negara
 * @property double $penghasilan
 * @property string $email
 *
 * The followings are the available model relations:
 * @property User $idU
 * @property Status $idS
 */
class Customer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_s, id_u, nama, kota, negara, penghasilan, email', 'required'),
			array('id_s, id_u', 'numerical', 'integerOnly'=>true),
			array('penghasilan', 'numerical'),
			array('nama, kota, negara, email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_c, id_s, id_u, nama, kota, negara, penghasilan, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idU' => array(self::BELONGS_TO, 'User', 'id_u'),
			'idS' => array(self::BELONGS_TO, 'Status', 'id_s'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_c' => 'Id C',
			'id_s' => 'Id S',
			'id_u' => 'Id U',
			'nama' => 'Nama',
			'kota' => 'Kota',
			'negara' => 'Negara',
			'penghasilan' => 'Penghasilan',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_c',$this->id_c);
		$criteria->compare('id_s',$this->id_s);
		$criteria->compare('id_u',$this->id_u);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('negara',$this->negara,true);
		$criteria->compare('penghasilan',$this->penghasilan);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
