<?php
/* @var $this CustomerController */
/* @var $data Customer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_c')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_c), array('view', 'id'=>$data->id_c)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_s')); ?>:</b>
	<?php echo CHtml::encode($data->id_s); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_u')); ?>:</b>
	<?php echo CHtml::encode($data->id_u); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota')); ?>:</b>
	<?php echo CHtml::encode($data->kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('negara')); ?>:</b>
	<?php echo CHtml::encode($data->negara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penghasilan')); ?>:</b>
	<?php echo CHtml::encode($data->penghasilan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	*/ ?>

</div>