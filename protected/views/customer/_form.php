<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_s'); ?>
		<?php echo $form->textField($model,'id_s'); ?>
		<?php echo $form->error($model,'id_s'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_u'); ?>

<?php echo $form->dropDownList($model, 'id_u', CHtml::listData(User::model()->findAll(), 'id_u', 'username'), array('empty'=>'–pilih User'));?>
		<?php echo $form->error($model,'id_u'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kota'); ?>
		<?php echo $form->textField($model,'kota',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'kota'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'negara'); ?>
		<?php echo $form->textField($model,'negara',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'negara'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'penghasilan'); ?>
		<?php echo $form->textField($model,'penghasilan'); ?>
		<?php echo $form->error($model,'penghasilan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->