<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_u')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_u), array('view', 'id'=>$data->id_u)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_l')); ?>:</b>
	<?php echo CHtml::encode($data->id_l); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paswd')); ?>:</b>
	<?php echo CHtml::encode($data->paswd); ?>
	<br />


</div>